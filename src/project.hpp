#pragma once

#include <string>
#include <vector>

#define COLOR_RESET "\033[0m"
#define COLOR_RED "\033[31m"
#define COLOR_GREEN "\033[32m"
#define COLOR_BLUE "\033[34m"
#define COLOR_MAGENTA "\033[35m"

class project
{
	private:
		std::string projectName;
		std::string projectPath;
		std::string binDir;
		std::string resourcesDir;
		std::string srcDir;
		std::string error;
		std::string oFlags;
		std::string cFlags;
		std::string includePathFlags;
		std::vector<std::string> libs;
	public:
		project();
		std::string get_name();
		std::string get_path();
		bool is_valid();
		
		void build();
		void clean();
		void debug();
		void run();
		void test();
		void install();
		
		void read_ma_file();
		void build_object_files();
		void build_executable();
		bool has_error();
		std::string get_error();
		void add_error(std::string error);
};