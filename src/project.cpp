#include "project.hpp"

#include <iostream>
#include <string.h>
#include <fstream>
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>
#include <vector>
#include <sys/stat.h>

project::project()
{
	// get the pwd and assume that's the project path:
	char cwd[1024];
	getcwd(cwd, sizeof(cwd));
	
	// store it in a c++ string so it's easy to manipulate
	projectPath = std::string(cwd);
	
	// refine projectPath to make sure a .ma file is there
	struct stat buffer;
	while(stat(std::string(projectPath+"/.ma").c_str(), &buffer) != 0) // while the .ma file does not exist
	{
		// go up one directory:
		int lastSlash = projectPath.find_last_of('/');
		projectPath = projectPath.substr(0, lastSlash);
		
		// make sure we didn't go all the way up the directory structure:
		if(projectPath.find_last_of('/') == 0)
		{
			std::cerr << "ERROR: no .ma file detected. Please create a file called '.ma' in the root directory of your c++ project." << std::endl;
			std::cerr << "This will allow you to build by running 'ma' from within a subdirectory." << std::endl;
			exit(1);
		}
	}
	
	// set the cwd to the projectPath so system calls are easier:
	int status = chdir(projectPath.c_str());
	if(status == -1)
	{
		std::cerr << "error while trying to call chdir() to set the cwd to " << projectPath << std::endl;
		exit(1);
	}
	
	// find where the last / is
	unsigned lastDirPos = projectPath.find_last_of('/');
	
	// just strip off the last part for the project name:
	projectName = projectPath.substr(lastDirPos + 1);
	
	// build some other dirs:
	binDir = projectPath + "/bin";
	srcDir = projectPath + "/src";
}
bool project::is_valid()
{
	bool valid = true;
	
	// make sure there's a bin dir:
	if(opendir(binDir.c_str()) == NULL)
	{
		std::cout << "ERROR: There should be a directory for binaries (" << binDir << ")" << std::endl;
		valid = false;
	}
	if(opendir(srcDir.c_str()) == NULL)
	{
		std::cout << "ERROR: There should be a directory for your .cpp and .hpp files (" << srcDir << ")" << std::endl;
		valid = false;
	}
	
	return valid;
}

std::string project::get_name()
{
	return projectName;
}
std::string project::get_path()
{
	return projectPath;
}

void project::build()
{
	std::cout << "Starting " << projectName << " build" << std::endl;
	
	build_object_files();
	build_executable();
	
	if(this->has_error())
	{
		std::cout << COLOR_RED << "Build failed. Errors below:" << COLOR_RESET << std::endl;
		std::cout << this->get_error() << std::endl;
	}
	else
	{
		std::cout << COLOR_GREEN << "Build finished successfully! Built executable is " << projectPath << "/" << projectName << COLOR_RESET << std::endl;
	}
}
void project::clean()
{
	std::cout << "Cleaning project " << projectName << ".." << std::endl;
	
	// get all files from the bin dir and delete them:
	DIR* dp = opendir(binDir.c_str());
	struct dirent* dirp;
	std::string filepath;
	if(dp != NULL)
	{
		while((dirp = readdir(dp)))
		{
			filepath = binDir + "/" + dirp->d_name;
			if(dirp->d_type == DT_REG) // if it's a file
			{
				// delete the file:
				unlink(filepath.c_str());
			}
		}
	}
	
	// delete the final executable:
	std::string execFile = projectPath + "/" + projectName;
	unlink(execFile.c_str());
}
void project::run()
{
	build();
	std::string cmd = get_path() + '/' + get_name();
	std::cerr << COLOR_BLUE << cmd << COLOR_RESET << std::endl;
	system(cmd.c_str());
}
void project::debug()
{
	oFlags += " -g";
	cFlags += " -g";
	build_object_files();
	build_executable();
}
void project::install()
{
	build_object_files();
	build_executable();
	
	// copy the executable to /usr/local/bin:
	std::string input;
	std::string path = "/usr/local/bin/";
	std::cout << "Do you want to copy the " << projectName << " executable to " << path << "? [y]es or [n]o ";
	std::cin >> input;
	if(input[0] == 'y'){
		std::string cmd = std::string("cp ") + projectName + ' ' + path + projectName;
		int status = system(cmd.c_str());
		if(status < 0){
			std::cerr << COLOR_RED << "error while attempting to install " << projectName << ": " << strerror(errno) << std::endl;
		}else{
			if(WIFEXITED(status)){
				int exit_status = WEXITSTATUS(status);
				if(exit_status == 0){
					std::cout << COLOR_GREEN << projectName << " installed successfully!" << COLOR_RESET << std::endl;
				}else{
					std::cout << COLOR_RED << "cp failed with exit status " << exit_status << COLOR_RESET << std::endl;
				}
			}else{
				std::cerr << COLOR_RED << "error: cp exited abnormally" << COLOR_RESET << std::endl;
			}
		}
	}else{
		std::cout << COLOR_RED << "Did NOT install." << COLOR_RESET << std::endl;
	}
}
void project::test()
{
	std::cout << "Building and running with the TEST symbol defined." << std::endl;
	oFlags += " -DTEST";
	cFlags += " -DTEST";
	build();
	
	// run it and get the exit status to determine if the tests were successful:
	std::string cmd = get_path() + "/" + projectName;
	std::cerr << COLOR_BLUE << cmd << COLOR_RESET << std::endl;
	int status = system(cmd.c_str());
	if(status < 0){
		std::cerr << COLOR_RED << "error while executing command: " << strerror(errno) << std::endl;
	}else{
		if(WIFEXITED(status)){
			int exit_status = WEXITSTATUS(status);
			if(exit_status == 0){
				std::cout << COLOR_GREEN << "Exit Status: 0. All tests pass!" << std::endl;
			}else{
				std::cout << COLOR_RED << "Exit Status: " << exit_status << ". Keep working at it!" << std::endl;
			}
		}else{
			std::cerr << COLOR_RED << "error, program exited abnormally" << std::endl;
		}
	}
}
void project::read_ma_file()
{
	// determine which platform we're on:
	int platform = 0; // default (linux)
	#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
	platform = 1; // 1 = windows
	#endif
	#ifdef __APPLE__
	platform = 2; // 2 = mac
	#endif
	
	// this will dictate whether the current line should run
	int current_platform = -1;
	
	std::ifstream mafile(std::string(get_path()+"/.ma"));
	std::string line;
	while(std::getline(mafile, line))
	{
		// handle comments (first character is #)
		if(line.length() == 0) continue; // handle blank line
		if(line.at(0) == '#') continue;
		
		// don't even look at this one if it was specified to only run on a platform we are not on:
		if(current_platform != -1 && current_platform != platform)
		{
			current_platform = -1; // reset this so it only affects 1 line
			continue;
		}
		
		// break it apart by the first space:
		int first_space = line.find_first_of(' ');
		std::string key = line.substr(0, first_space);
		std::string val = line.substr(first_space+1);
		
		// handle all known keys:
		if(key == "incpath")
		{
			includePathFlags += std::string(" -I"+val);
		}
		else if(key == "lib")
		{
			cFlags += std::string(" -l"+val);
		}
		else if(key == "libpath")
		{
			cFlags += std::string(" "+val);
		}
		else if(key == "flag")
		{
			oFlags += std::string(" "+val);
			cFlags += std::string(" "+val);
		}
		else if(key == "oflag")
		{
			oFlags += std::string(" "+val);
		}
		else if(key == "cflag")
		{
			cFlags += std::string(" "+val);
		}
		else if(key == "platform")
		{
			if(val == "linux") current_platform = 0;
			else if(val == "windows") current_platform = 1;
			else if(val == "mac") current_platform = 2;
			else
			{
				add_error("Error while parsing .ma file: platform '' not supported. try linux, windows, or mac instead.");
			}
			continue; // skip the current_platform reset
		}
		else
		{
			add_error("Error while parsing .ma file. Unknown key '"+key+"'");
		}
		current_platform = -1;
	}
}
void project::build_object_files()
{
	// first, build an object file for each file in src/
	DIR* dp = opendir(srcDir.c_str());
	struct dirent* dirp;
	std::string filepath;
	if(dp != NULL)
	{
		while((dirp = readdir(dp)))
		{
			filepath = srcDir + "/" + dirp->d_name;
			
			// if it ends in ".cpp" build a corresponding .o in bin
			if(dirp->d_type == DT_REG) // if it's a file
			{
				if(filepath.substr(filepath.length() - 4) == ".cpp")
				{
					size_t fileNameStart = filepath.find_last_of('/') + 1;
					size_t fileNameLength = filepath.find_last_of('.') - fileNameStart;
					std::string filename = filepath.substr(fileNameStart, fileNameLength);
					std::string cppfile = filepath;
					std::string ofile = binDir + "/" + filename + ".o";
					
					// make the command to build these .o files
					std::string cmd = "g++ -c " + cppfile + " -o " + ofile + oFlags;
					
					// run the build command to build the .o file:
					std::cout << COLOR_BLUE << cmd << COLOR_RESET << std::endl;
					int status = system(cmd.c_str());
					if(status != 0) // -1 means system() failed. >0 means compile failed.
					{
						add_error("Error while compiling object file '" + ofile + "'");
					}
				}
			}
			else if(dirp->d_type == DT_DIR) // if it's a dir
			{
				// if it's not ".." or "." build an object file from all .cpp files in this dir
				size_t fileNameStart = filepath.find_last_of('/') + 1;
				std::string dirName = filepath.substr(fileNameStart);
				
				if(dirName != ".." && dirName != ".")
				{
					// get a list of all cpp files, make a .cpp file in bin that includes all of them, then compile that and delete the .cpp
					std::vector<std::string> cppfiles;
					std::string ofile = binDir + dirName + ".o";
					
					// get a list of all cpp files in this subdir:
					DIR* subdp = opendir(srcDir.c_str());
					struct dirent* subdirp;
					std::string subfilepath;
					subdp = opendir(filepath.c_str());
					if(subdp != NULL)
					{
						while((subdirp = readdir(subdp)))
						{
							subfilepath = filepath + "/" + subdirp->d_name;
							std::string extension = subfilepath.substr(subfilepath.find_last_of(".") + 1);
							if(extension == "cpp")
							{
								cppfiles.push_back(subfilepath);
							}
						}
					}
					
					// if we have some cpp files, make a .cpp containing all of them, then compile that into a .o file:
					if(!cppfiles.empty())
					{
						std::string unityfilepath = srcDir + "/" + dirName + "/" + "_all.cpp";
						std::string objectfilepath = binDir + "/" + dirName + ".o";
						
						std::ofstream unityfile;
						unityfile.open(unityfilepath.c_str());
						
						if(unityfile.is_open())
						{
							// add an #include for all .cpp files in this sub dir
							unityfile << "// This file is generated by ma. you can delete it if you are reading this.\n";
							for(std::vector<std::string>::iterator it = cppfiles.begin(); it != cppfiles.end(); it++)
							{
								unityfile << "#include \"" << *it << "\"\n";
							}
							unityfile.close();
							
							// compile the object file:
							std::string cmd = "g++ -c " + unityfilepath + " -o " + objectfilepath + oFlags;
							std::cout << COLOR_BLUE << cmd << COLOR_RESET << std::endl;
							if(system(cmd.c_str()) == -1)
							{
								add_error("error while compiling sub directory object file '" + dirName + ".o'");
							}
							
							// delete the unity cpp file:
							if(remove(unityfilepath.c_str()) != 0)
							{
								add_error("Unable to remove unity file " + unityfilepath + " which is weird.");
							}
						}
						else
						{
							add_error("ERROR: failed to create file " + unityfilepath);
						}
					}
				}
			}
		}
	}
}
void project::build_executable()
{
	// if there was an error at some point before this, don't even try to do this:
	if(has_error())
	{
		return;
	}
	
	// get a space separated list of all .o files in the binDir
	std::string ofiles;
	
	DIR* dp = opendir(binDir.c_str());
	struct dirent* dirp;
	if(dp != NULL)
	{
		while((dirp = readdir(dp)))
		{
			std::string filename = dirp->d_name;
			if(filename != ".." && filename != ".")
			{
				if(filename.substr(filename.length() - 2) == ".o")
				{
					std::string filepath = binDir + "/" + dirp->d_name;
					ofiles += " " + filepath;
				}
			}
		}
	}
	
	// put together the compile command:
	std::string cmd = "g++ -o " + get_path() + '/' + get_name() + " " + ofiles + cFlags;
	
	// compile it!
	std::cout << COLOR_MAGENTA << cmd << COLOR_RESET << std::endl;
	int status = system(cmd.c_str());
	
	if(status == -1) // this means the system call failed.
	{
		add_error("system error while compiling final executable " + get_name());
	}
	else if(status > 0) // the compile failed and returned an error code. the compiler will have already printed out the errors
	{
		add_error("g++ error while building final executable " + get_name());
	}
}

bool project::has_error()
{
	if(error == "")
	{
		return false;
	}
	else
	{
		return true;
	}
}
std::string project::get_error()
{
	return error;
}
void project::add_error(std::string error)
{
	this->error += error + '\n';
}
