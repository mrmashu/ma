#include "project.hpp"

#include <iostream>

int main(int argc, char** argv)
{
	project p;
	if(p.is_valid())
	{
		p.read_ma_file();
		if(p.has_error()){
			std::cerr << "Errors while reading .ma file:" << std::endl;
			std::cerr << p.get_error() << std::endl;
			exit(1);
		}
		if(argc == 1){
			std::cout << "Building " << p.get_name() << " at " << p.get_path() << std::endl;
			p.build();
		}
		else if(argc == 2) // if we gave it 1 parameter
		{
			if(strcmp(argv[1], "clean") == 0){
				p.clean();
			}
			else if(strcmp(argv[1], "debug") == 0){
				p.debug();
			}
			else if(strcmp(argv[1], "run") == 0){
				p.run();
			}
			else if(strcmp(argv[1], "test") == 0){
				p.test();
			}
			else if(strcmp(argv[1], "install") == 0){
				p.install();
			}
			else{
				std::cout << "paramter " << argv[1] << " not supported. try clean, debug, run" << std::endl;
			}
		}
		else
		{
			std::cout << "unknown parameters. stopping." << std::endl;
		}
	}
	else
	{
		// invalid project, return 1 to show it didn't work
		std::cout << "Stopping due to invalid project. Errors below:" << std::endl;
		std::cout << p.get_error() << std::endl;
		return 1;
	}
	
	return 0;
}
